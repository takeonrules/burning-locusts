:PROPERTIES:
:ID:       12545F15-7633-46E8-AEC3-AD0EF3963BFB
:END:
#+title: Captain Jarek
#+FILETAGS: :burning-locusts:rpg:burning-wheel:npc:

Captain of [[id:C0E7DDC9-9E39-44F3-9496-EB370266E930][Lord Jade]]’s [[id:A35606CA-A786-46A4-BCB5-4C62239845D3][private security detail]].