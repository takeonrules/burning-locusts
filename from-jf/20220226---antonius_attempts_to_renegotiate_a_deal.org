:PROPERTIES:
:ID:       5CEE8244-4824-43B3-A719-79B22BF022A1
:END:
#+title: Antonius Attempts to Renegotiate a Deal
#+FILETAGS: :burning-locusts:rpg:burning-wheel:scene:

At [[id:7F79F69A-DE59-40E0-AF3C-CD6CDF111665][Ansidora’s large summer home]], [[id:AF52E196-0DB9-4F91-AF15-D7B7BF51F624][Frederico]] shares that [[id:C0E7DDC9-9E39-44F3-9496-EB370266E930][Lord Jade]] is assembling a private army of dwarves, to deal with the [[id:854D88A1-B777-4C65-875E-97B234338C2E][mercenary companies stealing stuff]].  Also, [[id:AF52E196-0DB9-4F91-AF15-D7B7BF51F624][Frederico]] doesn't know how to cook, so he won’t be joining the staff at [[id:77159A60-4285-4116-ACD4-6C0A42D01FAF][Adriano Faraldo’s Party]].

[[id:95297E1A-AB4D-4395-A286-62EA72854493][Ansidora]] shares about [[id:F87EADA9-A37C-4C56-9C9D-8C55A49C0D96][Old Roanie]] and asks that [[id:73B585B0-632C-441A-BAC6-E49B4D9A9A17][Antonius]] add [[id:622B711E-7A23-4F05-81BD-59FABBEB6CF2][Ginna]] to the painting she’s commissioned from [[id:73B585B0-632C-441A-BAC6-E49B4D9A9A17][Antonius]].

Here [[id:73B585B0-632C-441A-BAC6-E49B4D9A9A17][Antonius]] tries to renegotiate their previous arrangement, and we engage in a {{{i(Duel of Wits)}}}.

[[id:73B585B0-632C-441A-BAC6-E49B4D9A9A17][Antonius]]’s {{{i(Statements of Purpose)}}} is: “[[id:95297E1A-AB4D-4395-A286-62EA72854493][Ansidora]] will arrange a meeting with her, [[id:F87EADA9-A37C-4C56-9C9D-8C55A49C0D96][Old Roanie]], and myself before I even put paint to canvas for her [[id:E8DC59B8-6BDB-4E87-B90C-0742D29551FD][Tumul]].  After all, if [[id:2F5FB99F-3B79-4E48-B578-45E9E3DF48AD][Scintillante]], so does her [[id:E8DC59B8-6BDB-4E87-B90C-0742D29551FD][TumTum]].”

And [[id:95297E1A-AB4D-4395-A286-62EA72854493][Ansidora]]’s is “You’re trying to renege on a contract.  You’ll buy back my stuff from [[id:3B39D4B5-F505-4026-9891-07028B3977E4][Vadam]], paint [[id:622B711E-7A23-4F05-81BD-59FABBEB6CF2][Ginna]] and [[id:E8DC59B8-6BDB-4E87-B90C-0742D29551FD][Tumul]], and then I will take you to [[id:F87EADA9-A37C-4C56-9C9D-8C55A49C0D96][Old Roanie]].”

Both Ansidora and Antonius are primed for {{{i(Duel of Wits)}}}.  Each has a B6 {{{i(Will)}}} and a B5 in their primary skill: {{{i(Haggling)}}} and {{{i(Coarse Persuasion)}}}.  Antonius starts with a 7 {{{i(Body of Argument)}}}.  Ansidora spends 3 Persona (in service of a Belief) and starts with a 13 {{{i(Body of Argument)}}}.

We resolve the {{{i(Duel of Wits)}}}; [[id:73B585B0-632C-441A-BAC6-E49B4D9A9A17][Antonius]] loses and [[id:95297E1A-AB4D-4395-A286-62EA72854493][Ansidora]] has 6 remaining in her {{{i(Body of Argument)}}}}.{{{sidenote(I forgot to use this as an opportunity to start opening more of Antonius’s skills.)}}}

Concession: [[id:73B585B0-632C-441A-BAC6-E49B4D9A9A17][Antonius]] apologizes and offers to buy her family [[id:7B9A350C-2930-41B6-9678-628D1182BC1B][chronicles]].  Counter Offer: [[id:95297E1A-AB4D-4395-A286-62EA72854493][Ansidora]] counter-offers that the painting doesn't have to be first, but [[id:95297E1A-AB4D-4395-A286-62EA72854493][Ansidora]] wants her stuff back before they meet [[id:F87EADA9-A37C-4C56-9C9D-8C55A49C0D96][Old Roanie]].  That is the binding agreement.
