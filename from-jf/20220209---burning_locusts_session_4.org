:PROPERTIES:
:ID:       1D4C682B-9C71-4ADA-B5D1-C9E1D61895F2
:ROAM_REFS: https://takeonrules.com/2022/02/20/burning-locusts-session-4/
:SESSION_REPORT_DATE: 2022-01-19
:SESSION_REPORT_GAME: BURNING-WHEEL-GOLD
:SESSION_REPORT_LOCATION: via Discord and Roll20
:END:
:HUGO:
#+HUGO_BASE_DIR: ~/git/takeonrules.source/
#+HUGO_FRONT_MATTER_FORMAT: yaml
#+HUGO_SECTION: posts/2022
#+HUGO_CUSTOM_FRONT_MATTER: :type post
#+HUGO_CUSTOM_FRONT_MATTER: :layout post
#+HUGO_CUSTOM_FRONT_MATTER: :headline "Further Exposition as We Learn of an Art Theft Scheme and Hiring Personal Mercenaries"
#+HUGO_CUSTOM_FRONT_MATTER: :sessionReport '((date . 2022-01-19) (game .  BURNING-WHEEL-GOLD) (location . "via Discord and Roll20"))
:END:
#+TITLE: Burning Locusts: Session 4
#+SUBTITLE: Further Exposition as We Learn of an Art Theft Scheme and Hiring Personal Mercenaries
#+AUTHOR: Jeremy Friesen
#+FILETAGS: :session-report:rpg:burning-wheel:burning-locusts:

* Ansidora’s Rune Casting of Vadam

#+transclude: [[id:999D2E91-8628-4425-A82A-252B77824C52][Ansidora’s Rune Casting of Vadam]]

* Ansidora Meets with TumTum at the Mad Blossom Bistro

#+transclude: [[id:3B636F6E-8B80-4380-A8E5-B30C704DBDB1][Ansidora Meets with TumTum at the Mad Blossom Bistro]]

* Antonius Connecting with the Party Host

#+transclude: [[id:D5CA122D-92AE-4546-9C4E-A7EA7C3725E5][Antonius Connecting with the Party Host]]

* Frederico Meeting with Lady Jade

#+transclude: [[id:92C9A1E1-0544-4FB2-A13B-4BD4E62979A7][Frederico Meeting with Lady Jade]]

* Frederico's Seeking Plans to Vadam’s Place

#+transclude: [[id:2A516393-BCE0-4710-84D2-00E6C3D97BBA][Frederico's Seeking Plans to Vadam’s Place]]

* Antonius Attempts to Renegotiate a Deal

#+transclude: [[id:5CEE8244-4824-43B3-A719-79B22BF022A1][Antonius Attempts to Renegotiate a Deal]]

* Ansidora Meets with Stavros

#+transclude: [[id:5E35DE44-1AED-4FCB-8755-E95D9C9401C8][Ansidora Meets with Stavros]]

* Reflections

#+transclude: [[id:FA78B90F-183C-4BE2-8A8D-6256590A4A21][Reflections on Session 4 of Burning Locusts]]
