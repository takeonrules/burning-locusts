:PROPERTIES:
:ID:       7DABFA0F-5200-46E0-A494-F9EBFD23CBAD
:END:
#+title: Antonius and Maccio Have a Conversation Regarding the Arsonist
#+FILETAGS: :burning-locusts:rpg:burning-wheel:scene:

[[id:73B585B0-632C-441A-BAC6-E49B4D9A9A17][Antonius]], his mind full of things to consider, stays up all night.  In the morning he rouses [[id:32619736-C627-4097-9B8B-E268FE2C5C4E][Maccio]] to have a conversation.

[[id:73B585B0-632C-441A-BAC6-E49B4D9A9A17][Antonius]] says that it was clear [[id:32619736-C627-4097-9B8B-E268FE2C5C4E][Maccio]] recognized the arsonist.

“Yes, that was [[id:F8F97782-8390-4932-9DE5-8F760D898629][Lucianno]], my ex.  We used to be activists before, but he went crazy,” explained [[id:32619736-C627-4097-9B8B-E268FE2C5C4E][Maccio]].{{{sidenote(As a reminder\, [[id:32619736-C627-4097-9B8B-E268FE2C5C4E][Maccio]] has the Insurrectionist lifepath.)}}}

[[id:73B585B0-632C-441A-BAC6-E49B4D9A9A17][Antonius]] asks [[id:32619736-C627-4097-9B8B-E268FE2C5C4E][Maccio]] for help finding [[id:F8F97782-8390-4932-9DE5-8F760D898629][Lucianno]].  The arson and the assailants at [[id:A51A89AA-515A-4DAB-927B-65AF3F13646C][Adriano]]’s party must have been coordinated.

“I want to see what he has to say.  To hear his side.  Those who’ve felt unheard and oppressed will invariably find ways to be heard,” says [[id:73B585B0-632C-441A-BAC6-E49B4D9A9A17][Antonius]].