:PROPERTIES:
:ID:       50D9AF40-51D3-455E-BF63-D50690711FB6
:END:
:HUGO:
#+HUGO_FRONT_MATTER_FORMAT: yaml
#+HUGO_CUSTOM_FRONT_MATTER: :slug trait-vote-sessions-in-burning-wheel
#+HUGO_CUSTOM_FRONT_MATTER: :headline Sometimes Talking About the Game is More Important than Playing the Game
#+HUGO_CUSTOM_FRONT_MATTER: :date 2022-03-01 18:08:36 -0500
#+HUGO_CUSTOM_FRONT_MATTER: :type post
#+HUGO_CUSTOM_FRONT_MATTER: :layout post
#+HUGO_CUSTOM_FRONT_MATTER: :licenses '(all-rights-reserved)
#+HUGO_CUSTOM_FRONT_MATTER: :draft true
#+HUGO_CUSTOM_FRONT_MATTER: :org_id 50D9AF40-51D3-455E-BF63-D50690711FB6
#+HUGO_BASE_DIR: ~/git/takeonrules.source
#+HUGO_SECTION: posts/2022
:END:
#+title: Trait Vote Sessions in Burning Wheel
#+FILETAGS: :burning-wheel:session-analysis:rpg:rpg-reward:burning-locusts:
#+subtitle: Sometimes Talking About the Game is More Important than Playing the Game

For all of the sessions of {{{linkToGame(BURNING-WHEEL-GOLD)}}} I have played, {{{date(last week’s, 2022-02-23)}}} was my first {{{i(Trait Vote)}}} session.{{{sidenote(I think it’s about 40 as of writing this.)}}}

For those unfamiliar with the {{{i(Trait Vote)}}}, this is a session where you talk about the campaign, reflect on the actions of the characters, and adjust their traits.

After a conversation with one of the players on {{{date(2022-02-27, Sunday)}}}, I think {{{i(Trait Vote)}}} sessions may be more important than I first thought.  Yes they tweak the mechanics of your character…but wait there’s more.

* A Specific Trait Vote Session

In our {{{linkToSeries(burning-locusts)}}} campaign, we had a {{{i(Trait Vote)}}} after 7 sessions.

In that session, we found that our characters warranted traits that were too complicated for a single word.#+begin_sidenote
And traits like {{{linkToGame(BURNING-WHEEL-GOLD)}}}’s {{{i(Louis Wu)}}} telegraph that it’s okay for traits to be “insider” information.  After all, these traits are for the specific game at hand.
#+end_sidenote

In our trait vote, [[id:AF52E196-0DB9-4F91-AF15-D7B7BF51F624][Frederico]] picked up the character trait “Social Spider.”  He’s been slowly building a web of relationships always with the intention of creating a network.  Yet this wasn’t networking, but more spinning a web and waiting for things to get stuck.

[[id:73B585B0-632C-441A-BAC6-E49B4D9A9A17][Antonius]] picked up “Mover of Social Currents” and “Protective of Free Time.”
#+begin_sidenote
The former, a character trait.  The latter, a die trait; any social tests made against [[id:73B585B0-632C-441A-BAC6-E49B4D9A9A17][Antonius]] that request significant time from him are at +1 {{{abbr(OB)}}}.
#+end_sidenote
[[id:73B585B0-632C-441A-BAC6-E49B4D9A9A17][Antonius]] is open for novel opportunities but eager to protect his free time (not wanting to pay too high of an opportunity cost).  Yet whenever he interacts, he’s always eager to strike up a wager.  These wager’s are imbalanced.{{{sidenote(“If I win\, you share this information.  If you win\, I’ll help with your rat problem.”)}}}

Invariably, [[id:73B585B0-632C-441A-BAC6-E49B4D9A9A17][Antonius]] is seeking to bind people together with asymmetrical obligations.  This derives from the idea that barter is not about each trade being perfectly balanced, but instead each exchange comes with an imbalance.  One that leaves the other “owing” something.  And having the social contract to say “Yes, it’s okay to leave that obligation unresolved.”{{{sidenote(Something that flies in the face of American rugged individualism and gross autonomy.)}}}

I also pair this with the idea in Go that the goal is not to crush your opponent but to instead control one more square than them.  You’re trying to get just a bit more than the other person; and even when you lose it’s not humility but a small “accounting” error.  And [[id:73B585B0-632C-441A-BAC6-E49B4D9A9A17][Antonius]] sees that sometimes he must “give” just a bit more.

And then there’s [[id:95297E1A-AB4D-4395-A286-62EA72854493][Ansidora]]; she picked up “Always pay your dues”.  A {{{i(Call-on)}}} to help her better pay off her debts.  Which is great, because it telegraphs that she should go deeper into debt.

At a meta level, I find the nature of how [[id:73B585B0-632C-441A-BAC6-E49B4D9A9A17][Antonius]] and [[id:95297E1A-AB4D-4395-A286-62EA72854493][Ansidora]] view debt as something I hope to explore.

* But Why Is This Important?

As a player, while we were working through these traits, I felt a fond warmth towards the campaign.  We were talking about the game.  Sharing our observations.  Trying to really hone in on what makes the characters tick…and name it.

We were telling each other about our character.  Affirming who they were, and celebrating past moments.  And then adjusting a mechanical aspect of each character.

I found the session far more rewarding than any “You gain a level” moments of past games.{{{sidenote(Don’t get me wrong\, I love bringing a magic-user from 4^{th} to 5^{th} level.)}}}

This exercise of reflection brought me a new sense of energy.{{{sidenote(As well as some mechanical adjustments.)}}}  It also provided a “bookend” to the past sessions.

I think to those moments where I talk about past campaigns with friends.  Re-tell the shared memories of some fantastic tale to which only we were privy.  The trait vote, brought about that same sense of nostalgia while also providing energy (and mechanics) to move the campaign forward.

Had we not had this trait vote, I would’ve kept playing [[id:73B585B0-632C-441A-BAC6-E49B4D9A9A17][Antonius]], but wouldn’t know as much about him.  While I “play” [[id:73B585B0-632C-441A-BAC6-E49B4D9A9A17][Antonius]], he is in fact the product of what I’ve written, how others challenge him, and how the dice fall at the table.

Sincerely,

Pining and eager to play our next session.
