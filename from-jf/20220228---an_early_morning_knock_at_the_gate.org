:PROPERTIES:
:ID:       AA2A6F85-7DB4-457D-94F9-ED04AE9D0B6D
:END:
#+title: An Early Morning Knock at the Gate
#+FILETAGS: :burning-locusts:rpg:burning-wheel:scene:


The camera fades in with a view of [[id:2F5FB99F-3B79-4E48-B578-45E9E3DF48AD][Scintillante]] from [[id:73B585B0-632C-441A-BAC6-E49B4D9A9A17][Antonius]]'s courtyard.  The sun has yet to crest the horizon, but faint light shows [[id:73B585B0-632C-441A-BAC6-E49B4D9A9A17][Antonius]] preparing to setup a fresh canvas to begin a new painting.

As dawn breaks, there's another knock at the gate of [[id:07250203-CCDA-41D0-9485-8E9FF87ECE27][Villa di Mari]].

“One moment,” says [[id:73B585B0-632C-441A-BAC6-E49B4D9A9A17][Antonius]] as he sets the canvas on the easel and walks towards the gate.

He reaches for the gate, opening it and in recognition says, “Morning to you [[id:C0E7DDC9-9E39-44F3-9496-EB370266E930][Lord Jade]], it's a bit early for a game, so I must first ask what brings you to my villa?”

[[id:C0E7DDC9-9E39-44F3-9496-EB370266E930][Lord Jade]] a banker and member of the [[id:0B6050BD-4DDA-4B67-A61F-970B7EFB83E9][Hinterland Players]], a Go game club of established citizens of [[id:2F5FB99F-3B79-4E48-B578-45E9E3DF48AD][Scintillante]], enters the courtyard. {{{sidenote([[id:73B585B0-632C-441A-BAC6-E49B4D9A9A17][Antonius]] has the minor affiliation [[id:0B6050BD-4DDA-4B67-A61F-970B7EFB83E9][Hinterland Players]].  And [[id:AF52E196-0DB9-4F91-AF15-D7B7BF51F624][Frederico]] has the romantic relationship to [[id:0EEAB25F-BDA7-42AB-95B8-93DF47C3AC3A][Lady Jade]].)}}}

“Ah, my good [[id:73B585B0-632C-441A-BAC6-E49B4D9A9A17][Antonius]], I have a favor to ask of you.”

[[id:73B585B0-632C-441A-BAC6-E49B4D9A9A17][Antonius]] turns and meets [[id:C0E7DDC9-9E39-44F3-9496-EB370266E930][Lord Jade]]'s gaze, “How unexpected.  Before you ask, might you have time for a game.”

“Only a 9 by 9,” replies [[id:C0E7DDC9-9E39-44F3-9496-EB370266E930][Lord Jade]].

“Then a 9 by 9 it shall be.  What say we make this interesting? But first, I must know, is this favor something that would contravene my values?” asks [[id:73B585B0-632C-441A-BAC6-E49B4D9A9A17][Antonius]].

[[id:C0E7DDC9-9E39-44F3-9496-EB370266E930][Lord Jade]], shifting his stance responds, “I would hope not, but perhaps.”

“Now that is interesting.  What say we place a wager on this game.  If I win, you provide me with some hired bureaucrats who can at least get between me and all those that keep seeking Captain [[id:73B585B0-632C-441A-BAC6-E49B4D9A9A17][Antonius]] di Mari of the [[id:847B7B5D-9EBA-4A77-90E2-0B7823B1AFA0][Radiant Company]].  I grow tired of the «Oh captain, my captain» spouting suitors.  And I'll hear out your request.  If you win, I will honor your request, though I do ask for permission to abstain,” says [[id:73B585B0-632C-441A-BAC6-E49B4D9A9A17][Antonius]] as he gathers his 9 by 9 Go board and the bowls of white and black stones.

“Lord [[id:73B585B0-632C-441A-BAC6-E49B4D9A9A17][Antonius]], though you are my better, I accept these terms,” says Lord Jade as he settles down for a quick game.
#+begin_sidenote
[[id:C0E7DDC9-9E39-44F3-9496-EB370266E930][Lord Jade]] has a B3 {{{i(Strategy Games)}}} and [[id:73B585B0-632C-441A-BAC6-E49B4D9A9A17][Antonius]] a B4 {{{i(Strategy Games)}}}.  [[id:73B585B0-632C-441A-BAC6-E49B4D9A9A17][Antonius]] spends a {{{i(Persona)}}} and gets 3 successes (with 2 sixes).  [[id:C0E7DDC9-9E39-44F3-9496-EB370266E930][Lord Jade]] scores 3 successes.  [[id:73B585B0-632C-441A-BAC6-E49B4D9A9A17][Antonius]] spends a Fate and snags 3 more successes, for a total of 6 successes.
#+end_sidenote

[[id:73B585B0-632C-441A-BAC6-E49B4D9A9A17][Antonius]] chooses not to stamp out Lord Jade, but to give him a decisive yet not humiliating loss.

[[id:C0E7DDC9-9E39-44F3-9496-EB370266E930][Lord Jade]] says, “Well you have won, so those men shall be yours.”

“Wonderful, I cannot convey enough how disrupting those suitors are.  Thank you.  Now, please state your request.  I'm inclined to agree even without foreknowledge,” says [[id:73B585B0-632C-441A-BAC6-E49B4D9A9A17][Antonius]] as he looks to Lord Jade.

“Good [[id:73B585B0-632C-441A-BAC6-E49B4D9A9A17][Antonius]], you are an honorable man, and I do not wish to besmirch your integrity, but it has come to my attention that a man by the name of [[id:AF52E196-0DB9-4F91-AF15-D7B7BF51F624][Frederico]] has recently sought your services,” says [[id:C0E7DDC9-9E39-44F3-9496-EB370266E930][Lord Jade]] as he leans closer to [[id:73B585B0-632C-441A-BAC6-E49B4D9A9A17][Antonius]] to ensure more privacy on this early morning.

“[[id:C0E7DDC9-9E39-44F3-9496-EB370266E930][Lord Jade]], please describe this [[id:AF52E196-0DB9-4F91-AF15-D7B7BF51F624][Frederico]],” asks [[id:73B585B0-632C-441A-BAC6-E49B4D9A9A17][Antonius]].

[[id:C0E7DDC9-9E39-44F3-9496-EB370266E930][Lord Jade]] describes a man that matches [[id:AF52E196-0DB9-4F91-AF15-D7B7BF51F624][Frederico]]'s description.  The same [[id:AF52E196-0DB9-4F91-AF15-D7B7BF51F624][Frederico]] whom recently entered [[id:73B585B0-632C-441A-BAC6-E49B4D9A9A17][Antonius]]'s service.

“I have recently brought on that very [[id:AF52E196-0DB9-4F91-AF15-D7B7BF51F624][Frederico]] into my services,” says [[id:73B585B0-632C-441A-BAC6-E49B4D9A9A17][Antonius]] as he leans back to think.

[[id:C0E7DDC9-9E39-44F3-9496-EB370266E930][Lord Jade]] leans closer still and says, “This man has besmirched my honor, and I see that you yourself are in a bind.  I do not want you to renege on a contract but I also wish to protect my honor.”

[[id:73B585B0-632C-441A-BAC6-E49B4D9A9A17][Antonius]] rises from the game table, turns partially away from [[id:C0E7DDC9-9E39-44F3-9496-EB370266E930][Lord Jade]], and responds, “I am inclined to do as you ask, but I need some time to think on this.  And of course talk to [[id:AF52E196-0DB9-4F91-AF15-D7B7BF51F624][Frederico]].”

“That is as much as I could hope for my dear Lord [[id:73B585B0-632C-441A-BAC6-E49B4D9A9A17][Antonius]],” says [[id:C0E7DDC9-9E39-44F3-9496-EB370266E930][Lord Jade]] as he rises and extends a hand in friendship, “I will get those men to you forthwith.”

[[id:73B585B0-632C-441A-BAC6-E49B4D9A9A17][Antonius]] grasp [[id:C0E7DDC9-9E39-44F3-9496-EB370266E930][Lord Jade]]’s hand and says, “Thank you, I will know my course by end of day and send you word of my decision.  And please, sometime you and the lovely [[id:B67F7885-0456-4036-9CF5-A406B7EA0B07][Lady Jade]] should sit for a portrait.”

“You're too kind,” answers [[id:C0E7DDC9-9E39-44F3-9496-EB370266E930][Lord Jade]] as he turns and leaves the courtyard.

[[id:73B585B0-632C-441A-BAC6-E49B4D9A9A17][Antonius]] walks over to his blank canvas and sits in front of it.  His mind churns on his next play.
