:PROPERTIES:
:ID: takeonrules-2021-12-16-burning-locusts-session-1
:ROAM_REFS: https://takeonrules.com/2021/12/16/burning-locusts-session-1/
:SESSION_REPORT_DATE: 2021-12-15
:SESSION_REPORT_GAME: BURNING-WHEEL-GOLD
:SESSION_REPORT_LOCATION: via Discord and Roll20
:END:
#+title: Burning Locusts: Session 1 // Take on Rules
#+filetags: :session-report:burning-wheel:rpg:burning-locusts:
#+subtitle: Disruptions at Villa di Mari

The player characters are: [[id:AF52E196-0DB9-4F91-AF15-D7B7BF51F624][Frederico]], [[id:73B585B0-632C-441A-BAC6-E49B4D9A9A17][Antonius]], and [[id:95297E1A-AB4D-4395-A286-62EA72854493][Ansidora]].  You can read more about them from [[https://takeonrules.com/2021/12/10/burning-locusts-character-creation/][Burning Locusts: Character Creation]].

** A Game of Go to Size Each Other Up

#+transclude: [[id:AA486782-F54E-47B6-8211-EE93888853E8][A Game of Go to Size Each Other Up]]

** Antonius Agrees to Take on Frederico

#+transclude: [[id:C5E8192A-161A-48F0-B740-847FFB709929][Antonius Agrees to Take on Frederico]]

** An Evening Knock at the Gate

#+transclude: [[id:D6E28076-56AF-453D-BEFF-1F39E4250430][An Evening Knock at the Gate]]

** A Second Ask from Frederico

#+transclude: [[id:9ECB604B-A733-405D-BA26-D00EFC56C0A2][A Second Ask from Frederico]]

** An Early Morning Knock at the Gate

#+transclude: [[id:AA2A6F85-7DB4-457D-94F9-ED04AE9D0B6D][An Early Morning Knock at the Gate]]


** Casing the Ex-Husband’s House (for the First Time)

#+transclude: [[id:0ABC0E65-EE05-4AA8-948D-05FAD54C1E08][Casing the Ex-Husband’s House (for the First Time)]]


** Swords Drawn for Information

#+transclude: [[id:72B4F0B3-3F9F-4647-BE96-F62D1D380B17][Swords Drawn for Information]]

** Reflections

#+transclude: [[id:05E91E88-4C07-4AF6-91D4-5986BED10EFE][Reflections on Session 1 of Burning Locusts]]
