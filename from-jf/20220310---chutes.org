:PROPERTIES:
:ID:       3CE40FA5-B283-4FE4-B5E1-0A1DB1818E30
:END:
#+title: the Chutes
#+FILETAGS: :burning-locusts:rpg:burning-wheel:location:

A region in [[id:2F5FB99F-3B79-4E48-B578-45E9E3DF48AD][Scintillante]] where there is no guard presence.#+begin_sidenote
The lack of a guard presence is an established fact based on a failed {{{i(Guard-wise)}}} test in [[id:6381C449-367B-4A6C-B31E-32E8CF2F33CA][Burning Locusts: Session 10]].
#+end_sidenote
