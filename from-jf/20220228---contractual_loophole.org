:PROPERTIES:
:ID:       FF6F92DB-B594-4E96-804E-2062BF7EE5EE
:END:
#+title: contractual loophole
#+FILETAGS: :burning-locusts:rpg:burning-wheel:concept:

In the [[id:E9B418A3-8A33-4C9E-B4BB-6D26F42D1E02][The Scintillante Cicada Company Contract]] there is space for a loophole.

This was evident on [[id:73B585B0-632C-441A-BAC6-E49B4D9A9A17][Antonius]]’s reading.