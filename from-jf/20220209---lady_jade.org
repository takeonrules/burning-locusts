:PROPERTIES:
:ID:       0EEAB25F-BDA7-42AB-95B8-93DF47C3AC3A
:END:
#+title: Lady Jade
#+FILETAGS: :burning-locusts:rpg:burning-wheel:npc:

- Married to [[id:C0E7DDC9-9E39-44F3-9496-EB370266E930][Lord Jade]]
- Having a romantic affair with [[id:AF52E196-0DB9-4F91-AF15-D7B7BF51F624][Frederico]]